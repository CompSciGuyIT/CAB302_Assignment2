package asgn2Simulators;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ButtonPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 8645229832063987966L;
	
    private static final String RUN_SIM = "Run Simulation";
    private static final String SWITCH_CHARTS = "Switch Charts";

    private InputPanelsListener inputPanelsListener;
    
    private Timer timer;
    
    // Create instances of input classes to grab input data
    SimulationInputPanel SIP = new SimulationInputPanel();
    FareClassInputPanel FCIP = new FareClassInputPanel();
    
	public ButtonPanel() {
		
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		final JButton run = new JButton(RUN_SIM);
        run.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	boolean flag = true;
            	
            	String RNG_Seed = SIP.getRNG_Seed();
            	int RNG_Seed_Int = 0;
            	
            	String dailyMean = SIP.getDailyMean();
            	double dailyMean_Dbl = 0.0;
            	
            	String queueSize = SIP.getQueueSize();
            	int queueSize_Int = 0;
            	
            	String cancellation = SIP.getCancellation();
            	double cancellation_Dbl = 0.0;
            	
            	String first = FCIP.getFirst();
            	double first_Dbl = 0.0;
            	
            	String business = FCIP.getBusiness();
            	double business_Dbl = 0.0;
            	
            	String premium = FCIP.getPremium();
            	double premium_Dbl = 0.0;
            	
            	String economy = FCIP.getEconomy();
            	double economy_Dbl = 0.0;
            	
            	// Variable type validation checks
            	try {
            		RNG_Seed_Int = Integer.parseInt(SIP.getRNG_Seed());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "RNG Seed not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}
            	
            	try {
            		dailyMean_Dbl = Double.parseDouble(SIP.getDailyMean());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Daily mean not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		queueSize_Int = Integer.parseInt(SIP.getQueueSize());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Queue size not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		cancellation_Dbl = Double.parseDouble(SIP.getCancellation());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Cancellation value not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		first_Dbl = Double.parseDouble(FCIP.getFirst());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "First class value not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		business_Dbl = Double.parseDouble(FCIP.getBusiness());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Business class value not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		premium_Dbl = Double.parseDouble(FCIP.getPremium());
            	} catch (NumberFormatException ex) {
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Premium class value not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	try {
            		economy_Dbl = Double.parseDouble(FCIP.getEconomy());
            	} catch (NumberFormatException ex) {
            		flag = false;
            		JOptionPane.showMessageDialog(null,"Please input a valid value", "Economy class value not a valid type!",JOptionPane.ERROR_MESSAGE);
            	}

            	if (flag) {
	            	// Value validation checks
            		if (dailyMean_Dbl < 0) {
	            		JOptionPane.showMessageDialog(null,"Please enter a non-negative daily mean value", "Invalid daily mean value!",JOptionPane.ERROR_MESSAGE);
		            }
            		else if (queueSize_Int < 0) {
	            		JOptionPane.showMessageDialog(null,"Please enter a non-negative queue size", "Invalid queue size!",JOptionPane.ERROR_MESSAGE);
	            	}
            		else if (cancellation_Dbl < 0) {
	            		JOptionPane.showMessageDialog(null,"Please enter a non-negative cancellation value", "Invalid cancellation value!",JOptionPane.ERROR_MESSAGE);
	            	}
	            	else if ((first_Dbl + business_Dbl + premium_Dbl + economy_Dbl > 1) || (first_Dbl + business_Dbl + premium_Dbl + economy_Dbl < 1)) {
	            		JOptionPane.showMessageDialog(null,"Please input values that add up to 1", "Incorrect Fare Class values entered!",JOptionPane.ERROR_MESSAGE);
	            	}
	            	else{
		            	InputPanelsEvent ev = new InputPanelsEvent(this, RNG_Seed, dailyMean, queueSize, cancellation, 
		            			first, business, premium, economy);
						
						if (inputPanelsListener != null) {
							inputPanelsListener.inputEventOccurred(ev);
						}
	            	}
            	}
            }
        });

		final JButton sw_ch = new JButton(SWITCH_CHARTS);
        sw_ch.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                
                // finish button functionality

            }
        });
        
		
		setLayout(new FlowLayout(FlowLayout.CENTER));
		
		add(run);
		add(sw_ch);

	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void setInputListener(InputPanelsListener listener) {
		this.inputPanelsListener = listener;
	}
}
