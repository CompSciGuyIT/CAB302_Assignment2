package asgn2Simulators;

import java.util.EventListener;

public interface InputPanelsListener extends EventListener{

	public void inputEventOccurred(InputPanelsEvent e);
}
