package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;
import asgn2Aircraft.AircraftException;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;


public class A380Tests {
	
	private final String FLIGHT_CODE = "QF15";
	private final int BOOKING_TIME = 5;
	private final int DEPARTURE_TIME = 20;
	private final int CONFIRMATION_TIME = 10;
	private final int CANCELLATION_TIME = 15;
	private final int REFUSAL_TIME = 15;

	private Aircraft newAircraft;
	private Passenger newPassenger;
	
	private int firstCapacity = 3;
	
	/*
	 * Create a fresh aircraft object 
	 */
	@Before
	public void createAircraft() throws AircraftException {
		newAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME);
	}
	
	/*
	 * Create a fresh passenger object 
	 */
	@Before
	public void createPassenger() throws PassengerException {
		newPassenger = new First(BOOKING_TIME, DEPARTURE_TIME);
	}
	
	
	/*
	 * Testing the constructor to create a new aircraft
	 */
	@Test
	public void createNewAircraft() throws AircraftException {
		new A380(FLIGHT_CODE, DEPARTURE_TIME);
	}
	
	
	/*
	 * Testing the constructor by entering null flightCode
	 */
	@Test (expected = AircraftException.class)
	public void nullFlightCode() throws AircraftException {
		new A380(null, DEPARTURE_TIME);
	}
	
	
	/*
	 * Testing the constructor by entering zero in departure time
	 */
	@Test (expected = AircraftException.class)
	public void zeroDepartureTime() throws AircraftException {
		new A380(FLIGHT_CODE, 0);
	}
	
	/*
	 * Testing the constructor by entering negative integer in departure time
	 */
	@Test (expected = AircraftException.class)
	public void negativeDepartureTime() throws AircraftException {
		new A380(FLIGHT_CODE, -5);
	}
	
	
	/*
	 * Testing the constructor by entering negative seat first class value
	 */
	@Test (expected = AircraftException.class)
	public void negativeFirstClassSeatCapacity() throws AircraftException {
		@SuppressWarnings("unused")
		Aircraft aAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, -1, 0, 0, 0);
	}
	

	/*
	 * Testing the constructor by entering negative business class value
	 */
	@Test (expected = AircraftException.class)
	public void negativeBusinessClassSeatCapacity() throws AircraftException {
		@SuppressWarnings("unused")
		Aircraft aAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, 0, -1, 0, 0);
	}
	

	
	/*
	 * Testing the constructor by entering negative premium class value
	 */
	@Test (expected = AircraftException.class)
	public void negativePremiumClassSeatCapacity() throws AircraftException {
		@SuppressWarnings("unused")
		Aircraft aAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, 0, 0, -1, 0);
	}
	
	
	
	/*
	 * Testing the constructor by entering negative economy class value
	 */
	@Test (expected = AircraftException.class)
	public void negativeEconomyClassSeatCapacity() throws AircraftException {
		@SuppressWarnings("unused")
		Aircraft aAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, 0, 0, 0, -1);
	}
	
	
	/*
	 * Testing cancelBooking() method to ensure a confirmed booking is cancelled 
	 */
	@Test
	public void cancelBookingIsConfirmed() throws PassengerException, AircraftException {
		int beforeCancel = newAircraft.getNumPassengers();
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
		int afterAdd = newAircraft.getNumPassengers();
		
		assertEquals(beforeCancel + 1, afterAdd);
		
		newAircraft.cancelBooking(newPassenger, CANCELLATION_TIME);
		int afterCancel = newAircraft.getNumPassengers();
		
		assertEquals(beforeCancel, afterCancel);
	}
	
	
	/*
	 * Testing cancelBooking() method, passenger has not been confirmed
	 */
	@Test (expected = PassengerException.class)
	public void cancelBookingPassengerNotConfirmed() throws PassengerException, AircraftException {
		newAircraft.cancelBooking(newPassenger, CANCELLATION_TIME);
	}
	
	
	/*
	 * Testing cancelBooking() method, invalid cancellation time is below 0
	 */
	@Test (expected = PassengerException.class)
	public void cancelBookingInvalidNegativeCancellationTime() throws PassengerException, AircraftException {
		newPassenger.confirmSeat(BOOKING_TIME, DEPARTURE_TIME);
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
		newAircraft.cancelBooking(newPassenger, -5);	
	}
	
	
	/*
	 * Testing cancelBooking() method, invalid cancellation time after departure time
	 */
	@Test (expected = PassengerException.class)
	public void cancelBookingInvalidCancellationTimeAfterDepartureTime() throws PassengerException, AircraftException {
		newPassenger.confirmSeat(BOOKING_TIME, DEPARTURE_TIME);
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
		newAircraft.cancelBooking(newPassenger, 25);		
	}
	
	
	/*
	 * Testing cancelBooking() method, passenger not recorded in aircraft seating
	 */
	@Test (expected = AircraftException.class)
	public void cancelBookingPassengerNotRecorded() throws PassengerException, AircraftException {
		newPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		newAircraft.cancelBooking(newPassenger, CANCELLATION_TIME);
	}
	
	/*
	 * Testing confirmBooking() method by adding a passenger to the aircraft seating
	 */
	@Test
	public void confirmBookingAddPassenger() throws PassengerException, AircraftException {
		int beforeConfirm = newAircraft.getNumPassengers();
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
		int afterConfirm = newAircraft.getNumPassengers();
		assertEquals(beforeConfirm + 1, afterConfirm);
	}
	
	
	/*
	 * Testing confirmBooking() method, passenger is in an incorrect state
	 */
	@Test (expected = PassengerException.class)
	public void confirmBookingIncorrectPassengerState() throws PassengerException, AircraftException {
		newPassenger.refusePassenger(REFUSAL_TIME);
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
	}
	
	
	/*
	 * Testing confirmBooking() method with an invalid negative confirmation time
	 */
	@Test (expected = PassengerException.class)
	public void confirmBookingInvalidConfirmationTime() throws PassengerException, AircraftException {
		newPassenger.confirmSeat(BOOKING_TIME, DEPARTURE_TIME);
		newAircraft.confirmBooking(newPassenger, -6);
		assertTrue(BOOKING_TIME < DEPARTURE_TIME);
	}
	
	
	
	/*
	 * Testing confirmBooking() method with an invalid departure time
	 */
	@Test (expected = PassengerException.class)
	public void confirmBookingInvalidDepartureTime() throws PassengerException, AircraftException {
		newPassenger.confirmSeat(BOOKING_TIME, -5);
		newAircraft.confirmBooking(newPassenger, CONFIRMATION_TIME);
	}
	
	
	/*
	 * Testing confirmBooking() method with no seats available in fare class
	 */
	@Test (expected = AircraftException.class)
	public void confirmBookingNoFirstClassSeatsAvailableWithZeroSeats() throws PassengerException, AircraftException {
		Passenger Passenger1 = new First(BOOKING_TIME, DEPARTURE_TIME);
		
		newAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, 0, 0, 0, 0);
		newAircraft.confirmBooking(Passenger1, CONFIRMATION_TIME);
	}
	
	/*
	 * Testing confirmBooking() method with no seats available in fare class
	 */
	@Test (expected = AircraftException.class)
	public void confirmBookingNoFirstClassSeatsAvailableWithTwoSeats() throws PassengerException, AircraftException {
		Passenger Passenger1 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger Passenger2 = new First(BOOKING_TIME, DEPARTURE_TIME);
		Passenger Passenger3 = new First(BOOKING_TIME, DEPARTURE_TIME);
		
		newAircraft = new A380(FLIGHT_CODE, DEPARTURE_TIME, 2, 0, 0, 0);
		newAircraft.confirmBooking(Passenger1, CONFIRMATION_TIME);
		newAircraft.confirmBooking(Passenger2, CONFIRMATION_TIME);
		newAircraft.confirmBooking(Passenger3, CONFIRMATION_TIME);
	}
	
} // end A380Tests
